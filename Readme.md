# PriceListActicator

[![Maintenance](https://img.shields.io/badge/Maintained%3F-yes-green.svg)](https://gitlab.com/Georgi.Dimanov/pricelistactivator) [![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)]() [![Awesome Badges](https://img.shields.io/badge/badges-awesome-green.svg)]()

PriceListActivator is JavaFX based tool. Its main purpose is to execute SQL queries to a TISDetelina database.
Based on the options it will create a query to insert or update the prices already present in the price list.

# Features!

  - Plu ID range can be set to specify a set of id's to be updated.
  - NEW filters added range can now be set by choosing group or Common Supplier EIK.
  - Base price option available to choose from "Sell price" or "Buy price".
  - Percentage to increase or decrease the price list prices.
  - Keeps track of the last selected database file.

## Usage 
 - Step 1 : Database must be filled with the desired database file path.
 - Step 2 : Use the button "Test/Get lists" - it will verify if database is available and if it is it will populate the drop down list "Price lists :" with the names of the prices lists available in the database.
 - Step 3 : Selecting a plu range to be updated. Using the fields "From :" and "To :". Filed "From :" must be equal or greater than field "To :".
   /New added option to choose from two other filters. Only one filter can be used at a time./
 - Step 4 : Use radio buttons under "Base price :" tag to select with price to take as a base price when calculating the price list price.
 - Step 5 : Input the percentage in box "Percent % :". To decrease the price use negative percent. Example "-10%".
 - Step 6 : Optional. If check box is selected the program will try to update the prices of the selected plu ID range.
 - Step 7 : Press "Activate" button !
### Tech

PriceListActivator uses :

* [JavaFX] -Software platform for creating and delivering desktop applications
* [JDBC] - Java API to connect and execute the query with the database.
* [CSS] - Cascading Style Sheets is a style sheet language used for describing the presentation of a document written in a markup language like HTML.

### Installation

PriceListActivator requires [JDK](https://www.oracle.com/java/technologies/javase-jdk14-downloads.html/) v11+ to run.

**Free Software**

[JavaFX]: <https://en.wikipedia.org/wiki/JavaFX>
[JDBC]: <https://en.wikipedia.org/wiki/Java_Database_Connectivity>
[CSS]: <https://en.wikipedia.org/wiki/CSS>

