package Nodes;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class StatusNod extends Application {

    private static Label infoLabel = new Label("");

    @Override
    public void start(Stage stage) {

        stage.setTitle("Info");

        infoLabel.setStyle("-fx-font-size: 16px");

        Button okButton = new Button("Ok");
        okButton.setPrefSize(70,20);
        BorderPane buttonPane = new BorderPane();
        buttonPane.setCenter(okButton);
        BorderPane pane = new BorderPane();
        pane.setCenter(infoLabel);

        VBox vbox = new VBox(pane, buttonPane);
        vbox.setSpacing(40);

        okButton.setOnAction(actionEvent -> {
            Stage closeStage = (Stage) okButton.getScene().getWindow();
            closeStage.close();
        });

        Scene scene = new Scene(vbox, 250 , 150);
        stage.setScene(scene);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.show();

    }

    public void setInfoText(String text) {
        infoLabel.setText(text);
    }
}
