package Nodes;

import ButtonsAction.ActivateButtonAction;
import ButtonsAction.TestButtonAction;
import Tools.DbPathSaver;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;

public class AppMainNod extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }
    private static final DbPathSaver databaseFile = new DbPathSaver();
    private static Button test;
    private static Button activate;
    private static Button chooseDbButton;
    public static ComboBox listBox;
    public static CheckBox updateSelection;
    public static TextArea databasePath;
    public static TextArea fromInput;
    public static TextArea toInput;
    public static TextArea selectedGroup;
    public static TextArea selectedCustomer;
    public static TextArea percentInput;
    public static RadioButton sellPrice;
    public static RadioButton buyPrice;
    public static RadioButton usePluRange;
    public static RadioButton useGroup;
    public static RadioButton useSupplier;
    public static RadioButton lastSupplier;
    public static RadioButton commonSupplier;
    public ToggleGroup filterGroup;

    public void start(Stage stage) {

        stage.setTitle("PriceListActivator");

        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Firebird database files", "*.gdb"));

        generateRadioButtonSelectionFilter();

        GridPane databaseGrid = createDatabaseGrid();
        GridPane priceLists = createPriceGrid();
        GridPane pluRange = createPluRange();
        VBox selectionBox = createPluSelectionGrid();

        BorderPane activeButtonPane =  createActivateButton();

        VBox container = new VBox(databaseGrid,priceLists,selectionBox,pluRange,activeButtonPane);

        chooseDbButton.setOnAction(actionEvent -> {
            File selectedFile = fileChooser.showOpenDialog(stage);
                databasePath.setText(selectedFile.getAbsolutePath());
                databaseFile.overwritePathInFile(databasePath.getText());
        });

        test.setOnAction(actionEvent -> {
            databasePath.setText(databasePath.getText());
            TestButtonAction testButton = new TestButtonAction();
            testButton.activate();
        });

        activate.setOnAction(actionEvent -> {
            ActivateButtonAction activateButton = new ActivateButtonAction();
            activateButton.activate();
        });

        container.setStyle("-fx-padding: 10;" +
                "-fx-border-style: solid inside;" +
                "-fx-border-width: 2;" +
                "-fx-border-insets: 5;" +
                "-fx-border-radius: 5;" +
                "-fx-border-color: #00c6f7;");

        Scene scene = new Scene(container, 620, 530);

        stage.setScene(scene);
        stage.show();

    }

    private void generateRadioButtonSelectionFilter() {
        filterGroup = new ToggleGroup();
        usePluRange = new RadioButton("Use plu range.");
        useGroup = new RadioButton("Use Group.");
        useSupplier = new RadioButton("Use Supplier EIK.");

        usePluRange.setToggleGroup(filterGroup);
        useGroup.setToggleGroup(filterGroup);
        useSupplier.setToggleGroup(filterGroup);
    }

    private BorderPane createActivateButton() {

        String cssLayout = "-fx-border-color: black;\n" +
                "-fx-border-insets: 5;\n" +
                "-fx-border-width: 2;\n" +
                "-fx-border-style: solid;\n" +
                "-fx-padding: 3";

        BorderPane activeButtonPane = new BorderPane();
        activate = new Button("Activate");
        activate.setPrefSize(80, 30);
        activate.setStyle(cssLayout);
        activeButtonPane.setRight(activate);

        return activeButtonPane;
    }

    private VBox createPluSelectionGrid() {


        Label pluRange = new Label(" Plu Range");
        Label from = new Label(" From :    ");
        Label to = new Label("To : ");
        Label groupSelect = new Label("Select group :  ");
        Label customerSelect = new Label("Supplier EIK  :  ");

        String cssStyle = "-fx-padding: 5;" +
                "-fx-border-style: solid inside;" +
                "-fx-border-width: 2;" +
                "-fx-border-insets: 5;" +
                "-fx-border-radius: 5;" +
                "-fx-border-color: #00c6f7;";

        fromInput = new TextArea();
        fromInput.setPrefSize(100, 10);
        toInput = new TextArea();
        toInput.setPrefSize(100,10);
        selectedGroup = new TextArea();
        selectedGroup.setPrefSize(100,10);
        selectedCustomer = new TextArea();
        selectedCustomer.setPrefSize(100,10);

        lastSupplier = new RadioButton("Last Supplier");
        commonSupplier = new RadioButton("Common Supplier");
        ToggleGroup supplierSelectionGroup = new ToggleGroup();
        lastSupplier.setToggleGroup(supplierSelectionGroup);
        commonSupplier.setToggleGroup(supplierSelectionGroup);

        GridPane gridPluRange = new GridPane();
        gridPluRange.addRow(1,pluRange);
        gridPluRange.addRow(2, from, fromInput, to, toInput, usePluRange);
        gridPluRange.setHgap(10);
        gridPluRange.setStyle(cssStyle);
        GridPane groupPane = new GridPane();
        groupPane.addRow(1,groupSelect, selectedGroup, useGroup);
        groupPane.setStyle(cssStyle);
        groupPane.setHgap(10);
        GridPane customerPane = new GridPane();
        GridPane selectSupplierGrid = new GridPane();
        selectSupplierGrid.addRow(1,lastSupplier,commonSupplier);
        selectSupplierGrid.setHgap(10);
        selectSupplierGrid.setStyle("-fx-padding: 5;" +
                        "-fx-border-style: solid inside;" +
                        "-fx-border-width: 2;" +
                        "-fx-border-insets: 5;" +
                        "-fx-border-radius: 5;" +
                        "-fx-border-color: black;"
                );

        customerPane.addRow(1, customerSelect, selectedCustomer, selectSupplierGrid, useSupplier);
        customerPane.setStyle(cssStyle);
        customerPane.setHgap(10);

        return new VBox(gridPluRange,groupPane,customerPane);
    }

    private GridPane createPluRange() {
        GridPane gridPane = new GridPane();
        Label basePrice = new Label(  "Base price :");
        Label percentage = new Label("Percent % : ");
        Label infoPercent = new Label("-/+ %");
        updateSelection = new CheckBox("Select to UPDATE\nprice list prices.");
        percentInput = new TextArea();
        percentInput.setPrefSize(100,10);

        ToggleGroup priceSelectionGroup = new ToggleGroup();
        sellPrice = new RadioButton("Sell Price");
        buyPrice = new RadioButton("Buy Price");
        sellPrice.setToggleGroup(priceSelectionGroup);
        buyPrice.setToggleGroup(priceSelectionGroup);

        gridPane.addRow(1, basePrice);
        gridPane.addRow(2,sellPrice, buyPrice);
        gridPane.addRow(3, percentage, percentInput,infoPercent);
        gridPane.addRow(4,updateSelection);
        gridPane.setHgap(10);
        gridPane.setVgap(10);

        return gridPane;
    }

    private GridPane createPriceGrid() {
        GridPane gridPane = new GridPane();
        Label priceList = new Label(" Price Lists : ");
        listBox = new ComboBox();
        listBox.setMinSize(300, 30);

        gridPane.addRow(1, priceList, listBox);
        gridPane.setHgap(10);
        gridPane.setVgap(10);

        return gridPane;
    }

    private GridPane createDatabaseGrid() {
        GridPane gridPane = new GridPane();
        Label databaseLabel = new Label(" Database :");
        chooseDbButton = new Button("...");
        test = new Button("Test/Get lists");

        databasePath = new TextArea();
        if(!databaseFile.createDbSaveFile()) {
            databasePath.setText(databaseFile.getDbPathFromFile());
        }
        databasePath.setPrefSize(300,15);


        gridPane.addRow(1,databaseLabel, databasePath, chooseDbButton, test);
        gridPane.setHgap(10);
        gridPane.setVgap(10);

        return gridPane;
    }

    public void fillComboBox(String listName) {
        listBox.getItems().add(listName);
        listBox.getSelectionModel().selectFirst();
    }

    public void clearComboBox() {
        listBox.getItems().clear();
    }

}
