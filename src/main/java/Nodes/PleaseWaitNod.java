package Nodes;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;


public class PleaseWaitNod extends Application {

    private Stage waitStage;

    @Override
    public void start(Stage stage) {

        waitStage = stage;
        waitStage.setTitle("Please wait.");
        Label waitLabel = new Label("Please wait...");
//        waitLabel.setStyle("-fx-font-size: 14px");

        HBox hBox = new HBox(waitLabel);
        hBox.setAlignment(Pos.BASELINE_CENTER);

        Scene scene = new Scene(hBox, 180,50);
        waitStage.setScene(scene);
        waitStage.initModality(Modality.APPLICATION_MODAL);
        waitStage.show();
    }

    public void closeStage() {
        waitStage.close();
    }
}
