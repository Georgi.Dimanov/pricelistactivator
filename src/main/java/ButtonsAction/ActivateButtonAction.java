package ButtonsAction;

import Database.CheckConnection;
import Database.DbExecuteSql;
import Entity.PriceList;
import Nodes.AppMainNod;
import Nodes.StatusNod;
import javafx.stage.Stage;

public class ActivateButtonAction {

    private static int from;
    private static int to;
    private static double percentage;
    private static final DbExecuteSql dbExecuteSql = new DbExecuteSql();

    StatusNod statusNod = new StatusNod();
    CheckConnection validateConnection = new CheckConnection();

    public void activate() {
        if (AppMainNod.usePluRange.isSelected()) {
            activatePluRange();
        } else if (AppMainNod.useGroup.isSelected()) {
            activateGroup();
        } else if (AppMainNod.useSupplier.isSelected()) {
            activateSupplierEIK();
        } else {
            statusNod.setInfoText("Select plu range, group or\n Supplier EIK to use as filter");
            showStatusStage();
        }
    }

    private void activateSupplierEIK() {
        if(connectionValidation()) {
                if (dbExecuteSql.validateSupplierEIK(AppMainNod.databasePath.getText().trim(), AppMainNod.selectedCustomer.getText().trim())) {
                    if(validatePercentage()) {
                        if(validatePriceBoxIsSelected()) {
                            if (AppMainNod.lastSupplier.isSelected() || AppMainNod.commonSupplier.isSelected()) {
                                PriceList priceList = collectPriceListInformation();
                                if (priceList != null) {
                                    dbExecuteSql.activateSupplierList(priceList,
                                            AppMainNod.databasePath.getText().trim(),
                                            AppMainNod.updateSelection.isSelected(),
                                            AppMainNod.lastSupplier.isSelected());
                                }
                            } else {
                                statusNod.setInfoText("Select Supplier type!");
                                showStatusStage();
                            }
                        }
                    }
                } else {
                    statusNod.setInfoText("Supplier EIK not found in database!");
                    showStatusStage();
                }

        }
    }

    private void activatePluRange() {

        if (connectionValidation()) {
            if (validatePluRange()) {
                if (validatePluMinToMax()) {
                    if (validatePercentage()) {
                        if (validatePriceBoxIsSelected()) {
                            PriceList priceList = collectPriceListInformation();
                            if (priceList != null) {
                                dbExecuteSql.activatePriceList(priceList, AppMainNod.databasePath.getText().trim(), AppMainNod.updateSelection.isSelected());
                            }
                        }
                    }
                }
            }
        }
    }


    private void activateGroup() {

        if (connectionValidation()) {
            if (validateIsInteger(AppMainNod.selectedGroup.getText().trim())) {
                if (dbExecuteSql.validateSelectedGroup(AppMainNod.databasePath.getText().trim(),
                        Integer.parseInt(AppMainNod.selectedGroup.getText().trim()))) {
                    if (validatePercentage()) {
                        if (validatePriceBoxIsSelected()) {
                            PriceList priceList = collectPriceListInformation();
                            if (priceList != null) {
                                dbExecuteSql.activateGroupList(priceList, AppMainNod.databasePath.getText().trim(), AppMainNod.updateSelection.isSelected());
                            }
                        }
                    }
                } else {
                    statusNod.setInfoText("Selected group not found\nin database!");
                    showStatusStage();
                }
            }
        }
    }

    private boolean validateIsInteger(String input) {
        try {
            int toTest = Integer.parseInt(input);
            return true;
        } catch (Exception ignored) {
            statusNod.setInfoText("Only digits allowed!");
            showStatusStage();
            return false;
        }
    }

    private PriceList collectPriceListInformation() {
        int priceListId = getPriceListIDfromList();
        if (priceListId != 0) {
            PriceList priceList = getPriceListFromList();
            priceList.setPercentage(percentage);
            priceList.setBuyPrice(AppMainNod.buyPrice.isSelected());
            priceList.setFromPlu(from);
            priceList.setToPlu(to);
            return priceList;
        } else {
            statusNod.setInfoText("Error can't get price list ID!\nAborting Operation.");
            showStatusStage();
            return null;
        }
    }

    private PriceList getPriceListFromList() {
        String priceListName = (String) AppMainNod.listBox.getValue();
        PriceList priceList = new PriceList();
        for (PriceList lists : TestButtonAction.priceLists) {
            if (lists.getPriceListName().equals(priceListName)) {
                priceList = lists;
            }
        }
        return priceList;
    }

    private int getPriceListIDfromList() {
        String priceListName = (String) AppMainNod.listBox.getValue();
        int listId = 0;
        for (PriceList lists : TestButtonAction.priceLists) {
            if (lists.getPriceListName().equals(priceListName)) {
                listId = lists.getPriceListId();
            }
        }
        return listId;
    }

    private boolean connectionValidation() {
        if (validateConnection.checkConnection(AppMainNod.databasePath.getText().trim())) {
            return true;
        } else {
            statusNod.setInfoText("Database unavailable!");
            showStatusStage();
            return false;
        }
    }

    private boolean validatePriceBoxIsSelected() {
        if (AppMainNod.sellPrice.isSelected() || AppMainNod.buyPrice.isSelected()) {
            return true;
        } else {
            statusNod.setInfoText("Please select base price!");
            showStatusStage();
            return false;
        }
    }

    private boolean validatePercentage() {
        try {
            percentage = Integer.parseInt(AppMainNod.percentInput.getText().trim());
            return true;
        } catch (Exception ignored) {
            statusNod.setInfoText("Percentage error \nPlease use whole numbers");
            showStatusStage();
            return false;
        }
    }

    private boolean validatePluMinToMax() {
        if (from <= to) {
            return true;
        } else {
            statusNod.setInfoText("Starting plu id must be lower\n or equal to end id");
            showStatusStage();
            return false;
        }
    }

    private boolean validatePluRange() {

        try {
            from = Integer.parseInt(AppMainNod.fromInput.getText());
            to = Integer.parseInt(AppMainNod.toInput.getText());
            return true;
        } catch (Exception ignored) {
            statusNod.setInfoText("Plu range input error\nOnly digits allowed");
            showStatusStage();
            return false;
        }
    }

    private void showStatusStage() {
        Stage stage = new Stage();
        statusNod.start(stage);
    }

}
