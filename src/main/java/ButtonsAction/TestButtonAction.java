package ButtonsAction;

import Database.CheckConnection;
import Database.DbExecuteSql;
import Entity.PriceList;
import Nodes.AppMainNod;
import Nodes.StatusNod;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

public class TestButtonAction {

    private static final String FAIL_MSG = "Unable to connect to database!";
    private static final String SUCCESS_MSG = "Connection successful.";
    public static List<PriceList> priceLists = new ArrayList();

    AppMainNod appMainNod = new AppMainNod();
    CheckConnection validateConnection = new CheckConnection();
    StatusNod statusNod = new StatusNod();
    DbExecuteSql dbExecuteSql = new DbExecuteSql();

    public void activate() {

        String dbPath = AppMainNod.databasePath.getText().trim();

        if(validateConnection.checkConnection(dbPath)) {
            statusNod.setInfoText(SUCCESS_MSG);
            runInfoStage();
            priceLists.clear();
            priceLists = dbExecuteSql.getPriceListNames(dbPath);
            populatePriceListsInUI();
        } else {
           statusNod.setInfoText(FAIL_MSG);
           appMainNod.clearComboBox();
           runInfoStage();
        }

    }

    private void populatePriceListsInUI() {
        appMainNod.clearComboBox();
        for(PriceList list : priceLists) {
           appMainNod.fillComboBox(list.getPriceListName());
        }
    }

    private void runInfoStage() {
        Stage stage = new Stage();
        statusNod.start(stage);
    }

}
