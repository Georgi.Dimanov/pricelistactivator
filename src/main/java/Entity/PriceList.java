package Entity;

public class PriceList {

    private int priceListId;
    private String priceListName;
    private boolean buyPrice; // true -> buyPrice , false -> sellPrice
    private double percentage;
    private int fromPlu;
    private int toPlu;

    public int getPriceListId() {
        return priceListId;
    }

    public void setPriceListId(int priceListId) {
        this.priceListId = priceListId;
    }

    public String getPriceListName() {
        return priceListName;
    }

    public void setPriceListName(String priceListName) {
        this.priceListName = priceListName;
    }

    public boolean isBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(boolean buyPrice) {
        this.buyPrice = buyPrice;
    }

    public double getPercentage() {
        return percentage;
    }

    public void setPercentage(double percentage) {
        this.percentage = percentage;
    }

    public int getFromPlu() {
        return fromPlu;
    }

    public void setFromPlu(int fromPlu) {
        this.fromPlu = fromPlu;
    }

    public int getToPlu() {
        return toPlu;
    }

    public void setToPlu(int toPlu) {
        this.toPlu = toPlu;
    }
}
