package Entity;

public class Plu {

    private int pluId;
    private double pluBuyPrice;
    private double pluSellPrice;

    public int getPluId() {
        return pluId;
    }

    public void setPluId(int pluId) {
        this.pluId = pluId;
    }

    public double getPluBuyPrice() {
        return pluBuyPrice;
    }

    public void setPluBuyPrice(double pluBuyPrice) {
        this.pluBuyPrice = pluBuyPrice;
    }

    public double getPluSellPrice() {
        return pluSellPrice;
    }

    public void setPluSellPrice(double pluSellPrice) {
        this.pluSellPrice = pluSellPrice;
    }
}
