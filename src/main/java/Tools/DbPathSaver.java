package Tools;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;

public class DbPathSaver {

    private static final Logger logger = Logger.getLogger(DbPathSaver.class);
    private Path path = FileSystems.getDefault().getPath("").toAbsolutePath();
    private File dbFile = new File(path + "\\lastDatabase.txt");

    public boolean createDbSaveFile() {
        try {
            return dbFile.createNewFile();
        } catch (IOException ignored) {
            return false;
        }
    }

    public String getDbPathFromFile() {
        StringBuilder basePath = new StringBuilder();
        try {
            FileInputStream readFile = new FileInputStream(dbFile);
            int i = 0;
            while ((i = readFile.read()) != -1) {
                basePath.append((char) i);
            }
            readFile.close();
        } catch (IOException e) {
            logger.error("Unable to get database path from file!\n Error code :" + e.getMessage());
        }
        return basePath.toString();
    }

    public void overwritePathInFile(String dbPath) {
        try {
            FileOutputStream writeFile = new FileOutputStream(dbFile, false);
            byte[] bdFileToByte = dbPath.getBytes();
            writeFile.write(bdFileToByte);
            writeFile.close();
        } catch (IOException e) {
            logger.error("Unable to overwrite existing file!\n Error code : " + e.getMessage());
        }
    }

}
