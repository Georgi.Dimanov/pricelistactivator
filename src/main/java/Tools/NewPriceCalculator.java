package Tools;

import Nodes.AppMainNod;

public class NewPriceCalculator {

    public double calculatePrice(double basePrice) {
        int percentage = Integer.parseInt(AppMainNod.percentInput.getText().trim());

        if(percentage < 0) {
            return Math.round((basePrice / (1D + (percentage/100D)*-1))*100)/100D;
    } else
            return Math.round((basePrice  * (1D + percentage/100D))*100)/100D;
        }
    }
