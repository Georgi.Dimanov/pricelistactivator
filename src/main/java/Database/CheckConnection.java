package Database;

import java.sql.Connection;

public class CheckConnection {

    private static Connection testConnection;

    public boolean checkConnection(String dbPath) {

        testConnection = new DbConnection(dbPath).getConnection();

        return testConnection != null;
    }

    public void closeConnection() {
        try {
            testConnection.close();
        } catch (Exception e) {}
    }

}
