package Database;

import Entity.Plu;
import Entity.PriceList;
import Nodes.AppMainNod;
import Nodes.PleaseWaitNod;
import Tools.NewPriceCalculator;
import javafx.stage.Stage;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DbExecuteSql {

    private static final Logger logger = Logger.getLogger(DbExecuteSql.class);

    public List<PriceList> getPriceListNames(String dbPath) {

        List<PriceList> priceLists = new ArrayList<>();

        try (Connection connection = DbConnection.getInstance(dbPath).getConnection();
             Statement statement = connection.createStatement()
        ) {
            ResultSet resultSet = statement.executeQuery("Select NPR_ID, NPR_NAME from N_PRICE_LISTS");

            while (resultSet.next()) {
                PriceList currentPriceList = new PriceList();
                currentPriceList.setPriceListId(resultSet.getInt(1));
                currentPriceList.setPriceListName(resultSet.getString(2));
                priceLists.add(currentPriceList);
            }
        } catch (SQLException e) {
            logger.fatal("Unable to collect price list information from database. SQL Error code : " + e.getMessage());
        }

        return priceLists;

    }

    public void activateSupplierList(PriceList priceList, String dbPath,boolean update, boolean selectedSupplierEIK) {
        String supplierEIKColumn = "";

        if(selectedSupplierEIK) {
            supplierEIKColumn = "PLU_LAST_SUPPLIER";
        } else {
            supplierEIKColumn = "PLU_CMN_SUPPLIER";
        }

        PleaseWaitNod waitNod = new PleaseWaitNod();
        Stage stageToShow = new Stage();
        waitNod.start(stageToShow);
        try (Connection connection = DbConnection.getInstance(dbPath).getConnection();
             PreparedStatement statement = connection.prepareStatement("select PLU_NUMB, PLU_BUY_PRICE, PLU_SELL_PRICE from PLUES where " + supplierEIKColumn + " = ?")
        ) {

            statement.setString(1, AppMainNod.selectedCustomer.getText().trim());

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Plu currentPlu = new Plu();
                currentPlu.setPluId(resultSet.getInt(1));
                currentPlu.setPluBuyPrice(resultSet.getDouble(2));
                currentPlu.setPluSellPrice(resultSet.getDouble(3));
                if(!update) {
                    activateForCurrentPlu(priceList,currentPlu, dbPath);
                }  else {
                    updateForCurrentPlu(priceList,currentPlu, dbPath);
                }

                Thread.sleep(10);
            }

        } catch (SQLException e) {
            logger.fatal("Unable to collect plu Information form Database. SQL Error code : " + e.getMessage());
        } catch (InterruptedException ignored) {}
        waitNod.closeStage();
    }

    public void activatePriceList(PriceList priceList, String dbPath,boolean update) {

        PleaseWaitNod waitNod = new PleaseWaitNod();
        Stage stageToShow = new Stage();
        waitNod.start(stageToShow);
        try (Connection connection = DbConnection.getInstance(dbPath).getConnection();
             PreparedStatement statement = connection.prepareStatement("select PLU_NUMB, PLU_BUY_PRICE, PLU_SELL_PRICE from PLUES where PLU_NUMB between ? and ?")
        ) {

            statement.setInt(1, Integer.parseInt(AppMainNod.fromInput.getText().trim()));
            statement.setInt(2,Integer.parseInt(AppMainNod.toInput.getText().trim()));

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Plu currentPlu = new Plu();
                currentPlu.setPluId(resultSet.getInt(1));
                currentPlu.setPluBuyPrice(resultSet.getDouble(2));
                currentPlu.setPluSellPrice(resultSet.getDouble(3));
                 if(!update) {
                     activateForCurrentPlu(priceList,currentPlu, dbPath);
                 }  else {
                     updateForCurrentPlu(priceList,currentPlu, dbPath);
                 }

                Thread.sleep(10);
            }

        } catch (SQLException e) {
            logger.fatal("Unable to collect plu Information form Database. SQL Error code : " + e.getMessage());
        } catch (InterruptedException ignored) {}
        waitNod.closeStage();
    }

    public void activateGroupList(PriceList priceList, String dbPath,boolean update) {

        PleaseWaitNod waitNod = new PleaseWaitNod();
        Stage stageToShow = new Stage();
        waitNod.start(stageToShow);
        try (Connection connection = DbConnection.getInstance(dbPath).getConnection();
             PreparedStatement statement = connection.prepareStatement("select PLU_NUMB, PLU_BUY_PRICE, PLU_SELL_PRICE " +
                     "from PLUES " +
                     "LEFT JOIN N_PLUGROUPS " +
                     "ON plues.PLU_GROUP_ID = N_PLUGROUPS.PGRP_ID " +
                     " where PGRP_CODE = ?")
        ) {

            statement.setInt(1, Integer.parseInt(AppMainNod.selectedGroup.getText().trim()));

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Plu currentPlu = new Plu();
                currentPlu.setPluId(resultSet.getInt(1));
                currentPlu.setPluBuyPrice(resultSet.getDouble(2));
                currentPlu.setPluSellPrice(resultSet.getDouble(3));
                if(!update) {
                    activateForCurrentPlu(priceList,currentPlu, dbPath);
                }  else {
                    updateForCurrentPlu(priceList,currentPlu, dbPath);
                }

                Thread.sleep(10);
            }

        } catch (SQLException e) {
            logger.fatal("Unable to collect plu Information form Database. SQL Error code : " + e.getMessage());
        } catch (InterruptedException ignored) {}
        waitNod.closeStage();

    }

    public boolean validateSelectedGroup(String dbPath, int selectedGroup) {

        try (Connection connection = DbConnection.getInstance(dbPath).getConnection();
        PreparedStatement statement = connection.prepareStatement("select PGRP_CODE from N_PLUGROUPS where PGRP_CODE = ?")) {

            statement.setInt(1, selectedGroup);

            ResultSet result = statement.executeQuery();

            while (result.next()) {
                return true;
            }
            return false;

        } catch (SQLException e) {
            logger.error("Unable to read plu group from database. Check connection SQL Error code : " + e.getMessage());
            return false;
        }
    }

    public boolean validateSupplierEIK(String dbPath, String supplierEIK) {

        try (Connection connection = DbConnection.getInstance(dbPath).getConnection();
             PreparedStatement statement = connection.prepareStatement("select CUST_BULSTAT from CUSTOMERS_DATA where CUST_BULSTAT = ?")) {

            statement.setString(1, supplierEIK);

            ResultSet result = statement.executeQuery();

            while (result.next()) {
                return true;
            }
            return false;

        } catch (SQLException e) {
            logger.error("Unable to read customers from database. Check connection SQL Error code : " + e.getMessage());
            return false;
        }
    }

    private void updateForCurrentPlu(PriceList priceList, Plu currentPlu, String dbPath) {
        try(Connection connection = new DbConnection(dbPath).getConnection();
            PreparedStatement statement = connection.prepareStatement("UPDATE PRICE_LISTS SET PR_PRICE = ? \n" +
                    "WHERE PR_LISTID = ? AND PR_PLUNUMB = ?")
        ) {

            if(priceList.isBuyPrice()) {
                statement.setDouble(1,calculatePluPrice(currentPlu.getPluBuyPrice()));
            } else {
                statement.setDouble(1,calculatePluPrice(currentPlu.getPluSellPrice()));
            }
            statement.setInt(2,priceList.getPriceListId());
            statement.setInt(3, currentPlu.getPluId());

            statement.executeUpdate();

        } catch (SQLException e) {
            logger.fatal("Unable to insert values in priceList table. SQL Error code : " + e.getMessage());
        }
    }

    private void activateForCurrentPlu(PriceList priceList, Plu currentPlu, String dbPath) {

        try(Connection connection = new DbConnection(dbPath).getConnection();
            PreparedStatement statement = connection.prepareStatement("INSERT INTO PRICE_LISTS (PR_LISTID, PR_PLUNUMB,PR_PRICE, PR_CURR_ID) VALUES (?,?,?,?)")
        ) {
            statement.setInt(1,priceList.getPriceListId());
            statement.setInt(2, currentPlu.getPluId());

            if(priceList.isBuyPrice()) {
                statement.setDouble(3,calculatePluPrice(currentPlu.getPluBuyPrice()));
            } else {
                statement.setDouble(3,calculatePluPrice(currentPlu.getPluSellPrice()));
            }

            statement.setInt(4,1);

            statement.executeUpdate();

        } catch (SQLException e) {
            logger.fatal("Unable to insert values in priceList table. SQL Error code : " + e.getMessage());
        }

    }

    private double calculatePluPrice(double pluPrice) {
        NewPriceCalculator priceCalculator = new NewPriceCalculator();
        return priceCalculator.calculatePrice(pluPrice);
    }

}
