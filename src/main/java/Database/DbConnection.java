package Database;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DbConnection {

    private static Logger logger = Logger.getLogger(DbConnection.class);
    private static final String sourcePath = "jdbc:firebirdsql://localhost:3050/";
    private static DbConnection instance = null;
    private Connection connection;

    public DbConnection(String dbPath) {
        String databasePath = sourcePath + dbPath;
        try {
            Properties props = new Properties();
            props.setProperty("user", "SYSDBA");
            props.setProperty("password", "masterkey");
            props.setProperty("encoding", "WIN1251");
            this.connection = DriverManager.getConnection(databasePath, props);
        } catch (Exception e) {
            logger.error("Unable to connect to database at : "  + dbPath.replace("jdbc:firebirdsql://localhost:3050/", ""));
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public static synchronized DbConnection getInstance(String dbPath) throws SQLException {
        if (instance == null) {
            instance = new DbConnection(dbPath);
        } else if (instance.getConnection().isClosed()) {
            instance = new DbConnection(dbPath);
        }

        return instance;
    }

}
